var express = require("express");
var app = express();
require('dotenv').config();
var port = process.env.PORT || process.env.SERVER_PORT;

app.listen(port, function () {
  console.log(`Server is running at http://localhost:${port}`);
});

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); 

app.use(function (req, res, next) {
  // res.setHeader("Access-Control-Allow-Origin", process.env.REACT_APP_BASE_URL);
  res.setHeader("Access-Control-Allow-Origin", process.env.ALLOWED_ORIGIN);

  res.setHeader("Access-Control-Allow-Methods", "GET, POST");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type"); 

  next();
});

const ccxt = require("ccxt-xpr");
// const binance = new ccxt.binance({
//   apiKey: "FUiye4KfcatXtpMlRUaz2gnUGdUUBrKmmhrGS6H1gZpkEkvCX45d8rXWLzlU37Ei",
//   secret: "Afk4Ul3uZxvTXvkRX4xWOnq5JxcykjTFbGYyTqY08gC90ebHUZuh3xEcwEgMowwg",
//   options: {
//     adjustForTimeDifference: true,
//   },
// });

const binance1 = new ccxt.binance({
  apiKey: process.env.BINANCE_API_KEY1,
  secret: process.env.BINANCE_SECRET1,
  options: {
    adjustForTimeDifference: true,
  },
});

const binance2 = new ccxt.binance({
  apiKey: process.env.BINANCE_API_KEY2,
  secret: process.env.BINANCE_SECRET2,
  options: {
    adjustForTimeDifference: true,
  },
});
const okx = new ccxt.okx({
  apiKey: process.env.OKX_API_KEY,
  secret: process.env.OKX_SECRET,
  password: process.env.OKX_PASSWORD,
  options: {
    adjustForTimeDifference: true,
  },
  timeout: 300000, 
});
const mxc = new ccxt.mexc({
  apiKey: process.env.MXC_API_KEY,
  secret: process.env.MXC_SECRET,
  options: {
    adjustForTimeDifference: true,
  },
  timeout: 30000, // Set timeout to 30000 ms or 30 seconds
});
const getAccountInfo = async () => {
  try {
      const accountInfo = await okx.fetchBalance();
      console.log(accountInfo);
  } catch (error) {
      console.error('Error while retrieving account information:', error);
  }
};
const getAccountInfoMxc = async () => {
  try {
      const accountInfo = await mxc.fetchBalance();
      console.log(accountInfo);
  } catch (error) {
      console.error('Error while retrieving account information:', error);
  }
};
getAccountInfoMxc();
// Gọi hàm để lấy thông tin tài khoản
getAccountInfo();


// binance.setSandboxMode(true);
binance1.setSandboxMode(true);
// binance2.setSandboxMode(true);
// okx.setSandboxMode(true);

let currentAccount = "1"; 

app.post("/setCurrentAccount", async function (req, res) {
  const account = req.body.account;
  console.log(account);
  const accountMap = {
    "1": binance1,
    "2": binance2,
    "okx": okx,
    "mxc": mxc
  };

  if (accountMap[account]) {
    try {
      // Try to fetch the balance of the account
      await accountMap[account].fetchBalance();

      // If fetching the balance is successful, set the account as the current account
      currentAccount = account;
      console.log('current account' + currentAccount);
      res.send({ status: "success",  currentAccount: currentAccount , message: "The account has been updated successfully." });
    } catch (error) {
      // If fetching the balance fails, send an error response
      console.error('Error while retrieving account information:', error);
      res.send({ status: "error", currentAccount: currentAccount, message: "Error while retrieving account information: " + error.message });
    }
  } else {
    res.send({ status: "error", message: "This account is Invalid." });
  }
});
app.post("/balance", function (req, res) {
  const account = currentAccount; 
  const binance = account === "1" ? binance1 : account === "2" ? binance2 : account === "okx" ? okx : account === "mxc" ? mxc : null;   
  const newSymbol = req.body.newSymbol; // Get new symbol from client request
  binance
    .fetchBalance()
    .then((data) => {
      if (data && (data.free || data.info.balances.length > 0)) {
        const symbols = [];

        // Iterate over the free balances and add non-zero balances to the symbols array
        for (const symb in data.free) {
          if (data.free[symb] > 0) {
            symbols.push({
              symb,
              free: data[symb]?.free || 0,
              used: data[symb]?.used || 0,
              total: data[symb]?.total || 0,
            });
          }
        }

        // If new symbol exists in the balance data, add it to the symbols array
        if (newSymbol && data[newSymbol]) {
          symbols.push({
            symb: newSymbol,
            free: data[newSymbol]?.free || 0,
            used: data[newSymbol]?.used || 0,
            total: data[newSymbol]?.total || 0,
          });
        }

        res.json({
          result: true,
          symbols: symbols, 
          newSymbol: newSymbol // Add newSymbol to the response
        });
      } else {
        console.error('Failed to fetch balance: data or data.free is undefined');
        res.json({ result: false, message: 'Failed to fetch balance: data or data.free is undefined' });
      }
    })
    .catch((err) => {
      console.error("Failed to fetch balance:", err.message);
      res.json({ result: false, message: err.message });
    });
});



// app.post("/userSendBuyOrder", function (req, res) {
//   console.log(req.body); 
//   const binance = currentAccount === "1" ? binance1 : binance2; // Chọn đối tượng binance tương ứng

//   var amount = parseFloat(req.body.amount);
//   var symbol = req.body.symbol; // Get the symbol from the request body

//   binance.createMarketOrder(symbol, "buy", amount).then((data) => {
//     orderIds.push({ orderId: data.info.orderId, symbol: symbol }); // Store the symbol with the order ID
//     const price = data.info.fills && data.info.fills.length > 0 ? data.info.fills[0].price : null;

//     successfulOrders.push({
//       orderId: data.info.orderId,
//       amount: amount,
//       tokenCoin: symbol,
//       price: price,
//       endPrice: null,
//       status: data.status,
//       side: 'Buy',
//       account: currentAccount, 


//     });
//     console.log(successfulOrders);

//     console.log(`Buy order placed: ${amount} ${symbol}`); // Log the transaction
//     res.json(data);
//   }).catch((error) => {
//     console.error(error);
//     res.status(500).json({ error: 'Invalid order: Filter failure: NOTIONAL' });
//   });
// });

function updateSuccessfulOrders(orderId, status, endPrice) {
  const orderIndex = successfulOrders.findIndex(order => order.orderId === orderId);
  if (orderIndex !== -1) {
    console.log(`Updating order with ID ${orderId}`);
    successfulOrders[orderIndex].status = status;
    if (endPrice) {
      successfulOrders[orderIndex].endPrice = endPrice;
    }
    console.log(`Updated order: `, successfulOrders[orderIndex]);
  } else {
    console.log(`Order with ID ${orderId} not found`);
  }
}

// Route xử lý lệnh mua
app.post("/userSendBuyOrder", async function (req, res) {
  const account = currentAccount; 
  const binance = account === "1" ? binance1 : account === "2" ? binance2 : account === "okx" ? okx : account === "mxc" ? mxc : null;   
  var amount = parseFloat(req.body.amount);
  var symbol = req.body.symbol;
  var price = parseFloat(req.body.price); 

  try {
    const data = await binance.createLimitOrder(symbol, "buy", amount, price);
    console.log(data); 
    const orderId = data.id || data.clientOrderId; // Use either id or clientOrderId
    // orderIds.push({ orderId: orderId, symbol: symbol });
    const order = await binance.fetchOrder(orderId, symbol);
    console.log(order.status); 

    // orderIds.push({ orderId: data.info.orderId, symbol: symbol });

    successfulOrders.push({
      orderId: orderId,
      amount: amount,
      tokenCoin: symbol,
      price: price,
      endPrice: null,
      status: order.status,
      side: 'Buy',
      account: currentAccount,
    });
    console.log(successfulOrders);

    console.log(`Buy order placed: ${amount} ${symbol} at limit price: ${price}`);

// Start checking the status of the order every 10 seconds
const checkInterval = setInterval(async () => {
  const order = await binance.fetchOrder(orderId, symbol); // Use orderId here
  console.log(`Checking status of order ${orderId}: ${order.status}`);
  if (order.status === 'canceled') {
    clearInterval(checkInterval);
    console.log(`Order ${orderId} is canceled.`);
    updateSuccessfulOrders(orderId, 'canceled');
  }

  if (order.status === 'open') {
    return;
  }

  // If the order is filled, calculate the bought amount
  if (order.status === 'filled') {
    var boughtAmount = 0;
    successfulOrders.forEach((order) => {
      if (order.side === 'Buy' && order.status === 'filled' && order.tokenCoin === symbol) {
        boughtAmount += order.amount;
      }
    });

    console.log(`Bought amount: ${boughtAmount}, Order amount: ${amount}`);

    if (boughtAmount >= amount) {
      console.log(`Attempting to cancel buy order for ${amount} ${symbol}`);
      const cancelledData = await binance.cancelOrder(symbol, "buy", data.info.orderId);
      console.log(`Cancelled buy order for ${amount} ${symbol}`);
      // Update the order status to 'closed'
      updateSuccessfulOrders(orderId, 'closed');
    } else {
      console.log(`Not enough bought amount to cancel the order for ${amount} ${symbol}`);
    }
  }

  if (order.status === 'closed') {
    clearInterval(checkInterval);
    console.log(`Order ${data.info.orderId} is closed.`);
    updateSuccessfulOrders(data.info.orderId, 'closed');
  }
}, 10000)

res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Invalid order: Filter failure: NOTIONAL' });
  }
});


// app.post("/userSendSellOrder", function (req, res) {

//   const binance = currentAccount === "1" ? binance1 : binance2; 

//   var amount = parseFloat(req.body.amount);
//   var symbol = req.body.symbol; // Get the symbol from the request body

//   binance.createMarketOrder(symbol, "sell", amount).then((data) => {
//     orderIds.push(data.info.orderId); // Change this line
//     const price = data.info.fills && data.info.fills.length > 0 ? data.info.fills[0].price : null;

//     successfulOrders.push({
//       orderId: data.info.orderId,
//       amount: amount,
//       tokenCoin: symbol,
//       price: price, // price is not available for market orders
//       endPrice: null,
//       status: data.status,
//       side: 'Sell',
//       account: currentAccount, 

//     });
//     console.log(successfulOrders);

//     console.log(`Sell order placed: ${amount} ${symbol}`); // Log the transaction
//     res.json(data);
//   }).catch((error) => {
//     console.error(error);
//     res.status(500).json({ error: 'Invalid order: Filter failure: NOTIONAL' });
//   });
// });


app.post("/userSendSellOrder", async function (req, res) {
  const account = currentAccount; 
  const binance = account === "1" ? binance1 : account === "2" ? binance2 : account === "okx" ? okx : account === "mxc" ? mxc : null;   
  var amount = parseFloat(req.body.amount);
  var symbol = req.body.symbol;
  var price = parseFloat(req.body.price); 
  try {
    const data = await binance.createLimitOrder(symbol, "sell", amount, price);
    orderIds.push({ orderId: data.info.orderId, symbol: symbol });

    successfulOrders.push({
      orderId: data.info.orderId,
      amount: amount,
      tokenCoin: symbol,
      price: price,
      endPrice: null,
      status: data.status,
      side: 'Sell',
      account: currentAccount,
    });
    console.log(successfulOrders);

    console.log(`Sell order placed: ${amount} ${symbol} at limit price: ${price}`); // Log the transaction

    // Start checking the status of the order every 10 seconds
    const checkInterval = setInterval(async () => {
      const order = await binance.fetchOrder(data.info.orderId, symbol);
      console.log(`Checking status of order ${data.info.orderId}: ${order.status}`);
      
      if (order.status === 'canceled') {
        clearInterval(checkInterval);
        console.log(`Order ${data.info.orderId} is canceled.`);
        updateSuccessfulOrders(data.info.orderId, 'canceled');
      }

      if (order.status === 'open') {
        return;
      }

      if (order.status === 'filled') {
        updateSuccessfulOrders(data.info.orderId, 'closed');
      }

      if (order.status === 'closed') {
        clearInterval(checkInterval);
        console.log(`Order ${data.info.orderId} is closed.`);
        updateSuccessfulOrders(data.info.orderId, 'closed');
      }
    }, 10000); // Check every 10 seconds

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Invalid order: Filter failure: NOTIONAL' });
  }
});
var orderIds = [];

var successfulOrders = [];
app.post("/setSellOrders", function (req, res) {
  // const binance = currentAccount === "1" ? binance1 : binance2;
  const account = currentAccount; 
  const binance = account === "1" ? binance1 : account === "2" ? binance2 : account === "okx" ? okx : account === "mxc" ? mxc : null;   
  console.log('current account' + currentAccount);
  var totalAmount = parseFloat(req.body.amount);
  var numParts = parseInt(req.body.parts);
  var initialPrice = parseFloat(req.body.price);
  var priceIncrease = parseFloat(req.body.increase);
  var discount = parseFloat(req.body.discount); // new variable for the discount
  var currencyPair = req.body.currencyPair ; 
  var buyOption = req.body.buyOption; 
  console.log(buyOption);

  var partAmount = totalAmount / numParts;
  var currentPrice = initialPrice;
  var buyBackPercentage = 1 - discount / 100; // calculate the buy back percentage based on the discount
  

  var placeSellOrder = function () {
    if (numParts > 0) {
      numParts--;
      binance
        .createLimitSellOrder(currencyPair, partAmount, currentPrice)
        .then(async (data) => {
          currentPrice *= 1 + priceIncrease / 100;
          console.log(data); 

          // orderIds.push(data.info.orderId);
          const orderId = data.id || data.clientOrderId; // Use either id or clientOrderId
          const order = await binance.fetchOrder(orderId, currencyPair);
          console.log(order.status);
  
          successfulOrders.push({
            orderId: orderId,
            amount: partAmount,
            tokenCoin:currencyPair,
            price: currentPrice,
            endPrice: null,
            status: order.status,
            side: 'Sell',
            processed: false,
            account: currentAccount, 
          });
  
          console.log(successfulOrders);
          var openOrders = successfulOrders.filter(
            (order) => order.status === "open"
          );
          console.log("Open orders:", openOrders);
          res.send('Sell order placed successfully');

          // Start checking the status of the order every 10 seconds
          const checkInterval = setInterval(async () => {
            // const orderId = data.info.orderId;
            if (orderId) {
              const order = await binance.fetchOrder(orderId, currencyPair);
              console.log(`Checking status of order ${orderId}: ${order.status}`);
            
            if (order.status === 'canceled') {
              clearInterval(checkInterval);
              console.log(`Order ${data.info.orderId} is canceled.`);
              updateSuccessfulOrders(data.info.orderId, 'canceled');
            }
  
            if (order.status === 'open') {
              return;
            }
  
            if (order.status === 'filled') {
              updateSuccessfulOrders(data.info.orderId, 'closed');
            }
  
            if (order.status === 'closed') {
              clearInterval(checkInterval);
              console.log(`Order ${orderId} is closed.`);
              updateSuccessfulOrders(orderId, 'closed');
            }
          } else {
            console.error('Order ID is required to fetch an order');
          }
          }, 10000); // Check every 10 seconds
  
          // Check the status of the current order
          checkOrderStatus(orderId);
          console.log("Number of pending orders:", numParts);
  
        })
        .catch((error) => {
          console.log(error);
          if (error instanceof ccxt.InvalidOrder && error.message.includes('The minimum transaction volume cannot be less than：5USDT')) {
            res.status(400).send('The minimum transaction volume cannot be less than 5 USDT');
          } else if (error instanceof ccxt.DDoSProtection && error.message.includes('Invalid API-key, IP, or permissions for action.')) {
            res.status(403).send('Invalid API-key, IP, or permissions for action.');
          } else {
            res.status(500).send('An error occurred while placing the sell order');
          }
        });
    } else {
      console.log("All sell orders have been placed.");
      console.log("Successful and Running Orders:");
      console.log("Number of pending orders:", numParts);
    }
  };

var successfulBuyOrders = [];
var checkOrderStatus = function (orderId) {
  binance.fetchOrder(orderId, currencyPair).then((order) => {
    if (order.status === 'closed') {
      var orderIndex = successfulOrders.findIndex(o => o.orderId === orderId);
      if (orderIndex !== -1) {
        successfulOrders[orderIndex].processed = true;
        successfulOrders[orderIndex].endPrice = order.price;
      }


      // if (!successfulOrders.some(o => o.status === 'open' && !o.processed)) {
      //   placeSellOrder();
      // }
      if (numParts > 0) {
        placeSellOrder(); // Tiếp tục lệnh bán nếu còn số lượng phần
      }

      if (discount !== 0) {
        var openBuyOrder = successfulBuyOrders.find(o => o.status === 'open');
        if (openBuyOrder) {
          binance.cancelOrder(openBuyOrder.orderId, currencyPair).then(() => {
            placeBuyOrder(order.amount, successfulOrders[orderIndex].endPrice);
          });
        } else {
          placeBuyOrder(order.amount, successfulOrders[orderIndex].endPrice);
        }
      }
    } else if (order.status === 'canceled') {
      if (!successfulOrders.some(o => o.status === 'open' && !o.processed)) {
        placeSellOrder();
      }
    } else {
      setTimeout(() => checkOrderStatus(orderId), 5000);
    }
  }).catch((error) => {
    console.log("Error fetching order:", error);
  });
};

var placeBuyOrder = function (amount, sellPrice) {
  
  if (buyOption === 'buybyamount') {
    // Logic for buying back by the same amount
    var buyPrice = sellPrice * (1 - buyBackPercentage / 100);
    binance
      .createLimitBuyOrder(currencyPair, amount, buyPrice)
      .then((data) => {
        console.log("Buy order placed at price:", buyPrice);
        successfulOrders.push({
          orderId: data.id,
          amount: data.amount,
          tokenCoin: data.symbol,
          price: data.price,
          status: data.status,
          side: 'Buy',
          processed: false,
          account: currentAccount,
        });
        console.log(successfulBuyOrders);
      })
      .catch((error) => {
        console.log(error);
      });
  } else if (buyOption === 'buybyprice') {
    // Logic for buying back by the same price
    var buyAmount = amount * (1 + buyBackPercentage / 100);
    binance
      .createLimitBuyOrder(currencyPair, buyAmount, sellPrice)
      .then((data) => {
        console.log("Buy order placed at amount:", buyAmount);
        successfulOrders.push({
          orderId: data.id,
          amount: data.amount,
          tokenCoin: data.symbol,
          price: data.price,
          status: data.status,
          side: 'Buy',
          processed: false,
          account: currentAccount,
        });
        console.log(successfulBuyOrders);
      })
      .catch((error) => {
        console.log(error);
      });
  }
};

  placeSellOrder();
});



app.post("/setBuyOrders", function (req, res) {
  const binance = currentAccount === "1" ? binance1 : binance2;

  var totalAmount = parseFloat(req.body.amount);
  var numParts = parseInt(req.body.parts);
  var initialPrice = parseFloat(req.body.price);
  var priceDecrease = parseFloat(req.body.decrease);
  var currencyPair = req.body.currencyPair ; 
  var partAmount = totalAmount / numParts;
  var currentPrice = initialPrice;

  var placeBuyOrder = function () {
    var openBuyOrder = successfulOrders.find(o => o.status === 'open' && o.side === 'buy');
    if (openBuyOrder) {
      console.log('There is already an open buy order. Skipping...');
      return;
    }
    if (numParts > 0) {
      binance
        .createLimitBuyOrder(currencyPair, partAmount, currentPrice)
        .then((data) => {
          console.log(data);
          numParts--;
          currentPrice *= 1 - priceDecrease / 100;

          orderIds.push(data.info.orderId);

          successfulOrders.push({
            orderId: data.info.orderId,
            amount: partAmount,
            tokenCoin:currencyPair,
            price: currentPrice,
            endPrice: null,
            status: data.status,
            side: 'Buy',
            processed: false,
            account: currentAccount, 

            
          });

          console.log(successfulOrders);
          var openOrders = successfulOrders.filter(
            (order) => order.status === "open"
          );
          console.log("Open orders:", openOrders);

          // check status
          checkOrderStatus(data.info.orderId);
          console.log("Số lượng lệnh còn chờ đợi:", numParts);

        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      res.json({ message: "All buy orders have been placed." });
      console.log("Successful and Running Orders:");
      console.log("Số lượng lệnh còn chờ đợi:", numParts);

    }
  };
 
  var checkOrderStatus = function (orderId) {
    binance.fetchOrder(orderId, currencyPair).then((order) => {
      if (order.status === 'closed' || order.status === 'canceled') {
        placeBuyOrder();
      } else {
        setTimeout(() => checkOrderStatus(orderId), 10000);
      }
    });
  };


  placeBuyOrder();
});
  // app.get("/pendingOrdersCount", function (req, res) {
  //   var openOrders = successfulOrders.filter((order) => order.status === "open");
  //   console.log(`Number of pending orders: ${openOrders.length}`); // Log the number of pending orders
  //   res.json({ pendingOrdersCount: openOrders.length, numParts: numParts });
  // });
  app.get("/historyOrders", function (_, res) {
    var openOrders = successfulOrders.filter(order => order.status === 'open' && order.account === currentAccount);
    res.json(openOrders);
  });

  // app.get("/historyAllOrders", function (_, res) {
  //   res.json(successfulOrders);
  // });

  app.get("/historyAllOrders", function (_, res) {
    var allOrders = successfulOrders.filter(order => {
      return order.account === currentAccount;
    });
  
    res.json(allOrders);
  });
  app.get("/pendingOrders", function (_, res) {
    var pendingOrders = successfulOrders.filter(order => order.status === 'open');
    res.json(pendingOrders);
  });

  app.post("/cancelOrder/:orderId", function (req, res) {
    const account = currentAccount; 
    const binance = account === "1" ? binance1 : account === "2" ? binance2 : account === "okx" ? okx : account === "mxc" ? mxc : null;   
    const orderId = req.params.orderId;
    
    // Find the order in the successfulOrders array
    const order = successfulOrders.find(order => order.orderId === orderId);
    
    if (!order) {
        return res.status(404).json({ message: `Order ${orderId} does not exist.` });
    }

    const pair = order.tokenCoin;
    
    binance.cancelOrder(orderId, pair)
      .then(() => {
        // Remove the order from the array
        const orderIndex = successfulOrders.indexOf(order);
        if (orderIndex !== -1) {
          successfulOrders[orderIndex].status = 'cancelled';
          successfulOrders.splice(orderIndex, 1);
        }
        res.json({ message: `Order ${orderId} for pair ${pair} has been cancelled.` });
      })
      .catch((error) => {
        console.error(error);
        if (error instanceof ccxt.OrderNotFound) {
          res.status(404).json({ message: `Order ${orderId} for pair ${pair} does not exist.` });
        } else {
          res.status(500).json({ message: "Error cancelling order." });
        }
      });
  });

/*
app.post("/cancelOrder/:orderId", function (req, res) {
    const orderId = req.params.orderId;
    
    binance.cancelOrder(orderId, "BNB/USDT")
        .then(() => {
            // Find the order in the successfulOrders array
            const order = successfulOrders.find(order => order.orderId === orderId);
            // Update the status of the order
            if (order) {
                order.status = 'canceled';
            }
            res.json({ message: `Order ${orderId} has been cancelled.` });
        })
        .catch((error) => {
            console.error(error);
            if (error instanceof ccxt.OrderNotFound) {
                res.status(404).json({ message: `Order ${orderId} does not exist.` });
            } else {
                res.status(500).json({ message: "Error cancelling order." });
            }
        });
});
*/